def test_users(host):
    users = host.ansible.get_variables().get('users')
    for user in users:
        u = host.user(user)
        assert u.exists, f"User {user} should exist"

