# Структура проекта

```
.
├── files
│   ├── host-db-01
│   ├── host-web-01
│   │   └── web
│   └── my-host-ru
├── inventories
│   ├── inventory_docker.ini
│   └── inventory.yml
├── molecule
│   └── default
│       └── molecule.yml
├── old_project
│   ├── handlers
│   │   └── main.yml
│   ├── playbooks
│   │   └── nginx.yml
│   ├── README.md
│   └── templates
│       ├── html_file.j2
│       ├── nginx_conf.j2
│       ├── nginx_http.j2
│       └── nginx_https.j2
├── other
│   ├── extenion_uninst.yml
│   ├── hello_world.yml
│   ├── nginx_test.yml
│   ├── nginx_un.yml
│   ├── ping.yml
│   └── stop.yml
├── playbooks.yml
├── README.md
├── roles
│   ├── nginx
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── files
│   │   │   └── index.html
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   ├── molecule
│   │   ├── tasks
│   │   │   ├── configure.yml
│   │   │   ├── cron_job.yml
│   │   │   ├── install.yml
│   │   │   ├── main.yml
│   │   │   └── ssl.yml
│   │   ├── templates
│   │   │   ├── nginx_conf.j2
│   │   │   ├── nginx_http.j2
│   │   │   └── nginx_https.j2
│   │   └── vars
│   │       └── main.yml
│   └── users
│       ├── handlers
│       ├── tasks
│       │   └── main.yml
│       ├── templates
│       └── vars
│           └── main.yml
├── secrets.yml
└── testinfra_my.py
```
